//
//  Victory + api.swift
//  EvaGame
//
//  Created by Techwin on 17/03/21.
//

import Foundation
import UIKit

extension VictoryViewController {
    
   
    func apiCallForRewardsApi(){
        startAnimating(self.view)
        var coins = 0
        switch isComingFromLevel {
        case 0 : //easy
        coins = 50
        case 1 : //medium
            coins = 75
        case 2 : //hard
            coins = 100
        default :
            break
        }
        let params = ["coins": coins , "categoryid" : String(isComingFromLevel + 1)] as [String : Any]
        ApiManager.shared.Request(type: CoinsModel.self, methodType: .Post, url: BASE_URL + COINS_API , parameter: params) { (error, response, message, statusCode) in
            if statusCode == 200 {
                PrintToConsole("response of reward api \(String(describing: response))")
                    DispatchQueue.main.async {
                        self.setLevel(response?.coins ?? "")
                    }
            }else {
                if let msgStr = message {
                    Toast.show(message: msgStr, controller: self)
                }else {
                    Toast.show(message: SOMETHING_WENT_WRONG, controller: self)
                }
            }
        }
    }
    
}


