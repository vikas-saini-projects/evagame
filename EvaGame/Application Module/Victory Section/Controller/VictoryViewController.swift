//
//  VictoryViewController.swift
//  EvaGame
//
//  Created by Techwin on 02/03/21.
//

import UIKit

class VictoryViewController: UIViewController {

    @IBOutlet weak var bottomOfWhiteView: NSLayoutConstraint!
    @IBOutlet weak var TopOfWhiteView: NSLayoutConstraint!
    @IBOutlet weak var heightofStackView: NSLayoutConstraint!
    @IBOutlet weak var coinsView: UIView!
    @IBOutlet weak var CoinsLabel: UILabel!
    @IBOutlet weak var CongratulationLabel: UILabel!
    @IBOutlet weak var backgroundBlackView: UIView!
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var rewardsLabel: UILabel!
    @IBOutlet weak var rewardlabelBottom: NSLayoutConstraint!
    @IBOutlet weak var hightOfRewardsLabel: NSLayoutConstraint!
    @IBOutlet weak var heightOfVictoryLabel: NSLayoutConstraint!
    @IBOutlet weak var topOfVictory: NSLayoutConstraint!
    @IBOutlet weak var bottomOfStack: NSLayoutConstraint!
    @IBOutlet weak var GapBetweenCongoAndCoinsView: NSLayoutConstraint!
    @IBOutlet weak var GapBetweenVictoryAndCongo: NSLayoutConstraint!
    @IBOutlet weak var heightofCoinView: NSLayoutConstraint!
    
    
    var isComingFromLevel = 0 // 0 for easy , 1 for medium , 2 for hard
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK:- SETUP UI
    func setupUI(){
        self.CoinsLabel.text = ""
        self.backgroundBlackView.roundViewCorner(radius: 15)
        CoinsLabel.makeClickable(target: self, selector: #selector(speakCoins))
        rewardsLabel.makeClickable(target: self, selector: #selector(speakRewards))
        CongratulationLabel.makeClickable(target: self, selector: #selector(speakCongo))
        SetupForIphone()
        //commenting this line out as the user auth in not part of application anymore
        //apiCallForRewardsApi()
        //adding this due to same reason
        var coins = 0
        switch isComingFromLevel {
        case 0 : //easy
        coins = 50
        case 1 : //medium
            coins = 75
        case 2 : //hard
            coins = 100
        default :
            break
        }
        UserDefaults.standard.set(UserDefaults.standard.integer(forKey: UD_COINSCOLLECTED) + coins, forKey: UD_COINSCOLLECTED)
        DispatchQueue.main.async {
            self.setLevel(String(coins))
        }
    }
    
    @objc func speakCoins(){
        SpeakAText(CoinsLabel.text ?? "")
    }
    @objc func speakRewards(){
        SpeakAText(rewardsLabel.text ?? "")
    }
    @objc func speakCongo(){
        SpeakAText(CongratulationLabel.text ?? "")
    }
    
    //MARK:- SETUP FOR IPHONE
    func SetupForIphone(){
        if UIDevice.current.userInterfaceIdiom != .pad {
            topOfVictory.constant = -50
            heightOfVictoryLabel.constant = 70
            CongratulationLabel.font = CongratulationLabel.font.withSize(12.0)
            rewardsLabel.font = rewardsLabel.font.withSize(14)
            hightOfRewardsLabel.constant = 35
            rewardlabelBottom.constant = -17.5
            heightofCoinView.constant = 65
            heightofStackView.constant = 60
            TopOfWhiteView.constant = 40
            bottomOfWhiteView.constant = 10
            CoinsLabel.font = CoinsLabel.font.withSize(12.0)
            GapBetweenVictoryAndCongo.constant = 20
            GapBetweenCongoAndCoinsView.constant = 30
            bottomOfStack.constant = -30
        }
    }
    
    
    //MARK:- VIEW DID LAYOUT SUBVIEW
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async {
            self.coinsView.giveShadowAndRoundCorners(shadowOffset: CGSize.zero, shadowRadius: 15, opacity: 0.7, shadowColor: .lightGray, cornerRadius: 15)
        }
    }
    
    //MARK:- SET LEVEL
    func setLevel(_ coins : String){
        var string = ""
        switch isComingFromLevel {
        case 0: string = "Congratulation, you completed this track on easy!"
        case 1: string = "Congratulation, you completed this track on medium!"
        case 2: string = "Congratulation, you completed this track on hard!"
        default:break
        }
        self.CongratulationLabel.text = string
        self.CoinsLabel.text = "\(coins) Ballons"
     
    }
    
    //MARK:- BUTTON ACTIONS
    @IBAction func HomeButtonAction(_ sender: Any) {
        if let vc = R.storyboard.chooseTheme.chooseThemeViewController() {
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    @IBAction func PlayButtonAction(_ sender: Any) {
        if let vc = R.storyboard.chooseTheme.chooseThemeViewController() {
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
}
