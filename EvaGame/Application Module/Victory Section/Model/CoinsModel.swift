//
//  CoinsModel.swift
//  EvaGame
//
//  Created by Techwin on 17/03/21.
//

import Foundation
import UIKit

// MARK: - CoinsModel
struct CoinsModel: Codable {
    let status: Int
    let message, coins, method, success: String?
}

