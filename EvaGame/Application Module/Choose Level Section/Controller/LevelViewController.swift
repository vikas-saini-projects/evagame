//
//  LevelViewController.swift
//  EvaGame
//
//  Created by Admin on 01/03/21.
//

import UIKit

class LevelViewController: UIViewController {
    
    //MARK:- OUTLETS
   
    @IBOutlet weak var GapConstraint: NSLayoutConstraint!
    @IBOutlet weak var BackButtonheight: NSLayoutConstraint!
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var BackgroundWhiteView: UIView!
    @IBOutlet weak var Hard: UIButton!
    @IBOutlet weak var easyButton: UIButton!
    @IBOutlet weak var mediumButton: UIButton!
    @IBOutlet weak var chooseACategoryLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var BottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightoFTopLabel: NSLayoutConstraint!
    
    var ThemeID = ""
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        chooseACategoryLabel.text = ""
//        chooseACategoryLabel.makeClickable(target: self, selector: #selector(speakChooseCategory))
    }
    
//    @objc func speakChooseCategory(){
//        SpeakAText(chooseACategoryLabel.text ?? "")
//    }
    
    //MARK:- SETUP UI
    func setupUI(){
        DispatchQueue.main.async {
            self.BackgroundWhiteView.layer.cornerRadius = 15
            self.setupButton(self.easyButton)
            self.setupButton(self.mediumButton)
            self.setupButton(self.Hard)
            self.setupButton(self.BackButton)
            self.setupForIphone()
        }
    }
    
    //MARK:- SETUP FOR IPHONE
    func setupForIphone(){
        if UIDevice.current.userInterfaceIdiom == .phone {
        BackButtonheight.constant = 30
        self.heightoFTopLabel.constant = 40
        chooseACategoryLabel.font = chooseACategoryLabel.font.withSize(22)
        GapConstraint.constant = 20
        BottomConstraint.constant = 60
        stackView.spacing = 08
        for button in stackView.arrangedSubviews {
            if let button = button as? UIButton {
                button.titleLabel?.font = button.titleLabel?.font.withSize(16)
            }
        }
        }
        
    }
    
    //MARK:- SETUP BUTTONS
    func setupButton(_ button : UIButton){
        DispatchQueue.main.async {
            button.layer.cornerRadius = button.bounds.height / 2 
            button.layer.borderColor = UIColor.white.cgColor
            button.layer.borderWidth = 1
        }
    }
    
    //MARK:- BUTTON ACTIONS
    @IBAction func HardAction(_ sender: Any) {
        Hard.backgroundColor = GREENISH_COLOR
        mediumButton.backgroundColor = DARK_SKYBLUE_COLOR
        easyButton.backgroundColor = DARK_SKYBLUE_COLOR
        
        if let vc = R.storyboard.hardLevel.hardLevelViewController() {
            vc.ThemeID = self.ThemeID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func mediumAction(_ sender: Any) {
        Hard.backgroundColor = DARK_SKYBLUE_COLOR
        mediumButton.backgroundColor = GREENISH_COLOR
        easyButton.backgroundColor = DARK_SKYBLUE_COLOR
        
        if let vc = R.storyboard.mediumPuzzle.mediumPuzzleViewController(){
            vc.ThemeID = self.ThemeID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func easyActiion(_ sender: Any) {
        Hard.backgroundColor = DARK_SKYBLUE_COLOR
        mediumButton.backgroundColor = DARK_SKYBLUE_COLOR
        easyButton.backgroundColor = GREENISH_COLOR
        
        if let vc = R.storyboard.easyStoryboard.easyLevelViewController(){
            vc.ThemeID = self.ThemeID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func GoBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
