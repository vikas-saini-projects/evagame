//
//  Medium + api.swift
//  EvaGame
//
//  Created by Techwin on 17/03/21.
//

import Foundation
import UIKit
import SDWebImage

extension MediumPuzzleViewController {
    
   
    func apiCallForMediumGameApi(){
        startAnimating(self.view)
        let params = ["themeid": ThemeID , "categoryid" : CategoryID.Medium.rawValue]
        ApiManager.shared.Request(type: EasyGameModel.self, methodType: .Post, url: BASE_URL + EASY_AND_MEDIUM_GAME_API , parameter: params) { (error, response, message, statusCode) in
            if statusCode == 200 {
                if (response?.data) != nil {
                PrintToConsole("response of playGame Medium api \(String(describing: response))")
                    DispatchQueue.main.async {
                        self.GameContentArray = response?.data ?? []
                        if let randomElement = self.GameContentArray.randomElement() {
                            self.imageView.sd_imageIndicator  = SDWebImageActivityIndicator.grayLarge
                            self.imageView.sd_setImage(with: URL(string: BASE_URL + (randomElement.image ?? "")), placeholderImage: nil, options: [.highPriority]) { (Image, error, cache, type) in
                                guard error == nil else {
                                    Toast.show(message: "Error \(error?.localizedDescription ?? "with Image Url!")", controller: self)
                                    return
                                }
                                self.InitialImagesArray =  slice(image: Image!, into: 3)
                                self.FinalImagesArray = self.InitialImagesArray.shuffled()
                                self.collectionview.dragInteractionEnabled = true
                                self.collectionview.delegate = self
                                self.collectionview.dataSource = self
                                self.collectionview.dragDelegate = self
                                self.collectionview.dropDelegate = self
                            }
                        }
                    }
               
                }else {
                    Toast.show(message: DATA_NOT_FOUND, controller: self)
                }
            }else {
                if let msgStr = message {
                    Toast.show(message: msgStr, controller: self)
                }else {
                    Toast.show(message: SOMETHING_WENT_WRONG, controller: self)
                }
            }
        }
    }
    
}
