//
//  MediumPuzzle + CollectionView.swift
//  EvaGame
//
//  Created by Techwin on 02/03/21.
//

import Foundation
import UIKit


//MARK:- EXTENSION FOR COLLECTION VIEW DELEGATE AND DATA SOURCE

extension MediumPuzzleViewController : UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return FinalImagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.imageCollectionViewCell, for: indexPath)!
        cell.particularImage.image = self.FinalImagesArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout  collectionViewLayout: UICollectionViewLayout, sizeForItemAt  indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width / 3, height:    collectionView.bounds.height / 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}


//MARK:- EXTENSION FOR COLLECTION VIEW DRAG DELEGATE

extension MediumPuzzleViewController:  UICollectionViewDragDelegate {
    
    func collectionView(_ collectionView: UICollectionView,  itemsForBeginning session: UIDragSession, at indexPath: IndexPath)  -> [UIDragItem] {
        let item = self.FinalImagesArray[indexPath.row]
        let itemProvider = NSItemProvider(object: item )
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = dragItem
        return [dragItem]
    }
}


//MARK:- EXTENSION FOR COLLECTION VIEW DROP DELEGATE

extension MediumPuzzleViewController:  UICollectionViewDropDelegate {
    
    func collectionView(_ collectionView: UICollectionView,   dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        if collectionView.hasActiveDrag {
            return UICollectionViewDropProposal(operation: .move, intent:    .insertAtDestinationIndexPath)
        }
        return UICollectionViewDropProposal(operation: .forbidden)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        
        var destinationIndexPath: IndexPath
        if let indexPath = coordinator.destinationIndexPath {
            destinationIndexPath = indexPath
        } else {
            let row = collectionView.numberOfItems(inSection: 0)
            destinationIndexPath = IndexPath(item: row - 1 , section: 0)
        }
        
        if coordinator.proposal.operation == .move {
            self.reorderItems(coordinator: coordinator,  destinationIndexPath: destinationIndexPath, collectionView:   collectionView)
            self.collectionview.reloadData()
        }
    }
    
    fileprivate func reorderItems(coordinator:   UICollectionViewDropCoordinator, destinationIndexPath:IndexPath,   collectionView: UICollectionView) {
        if let item = coordinator.items.first,
           let sourceIndexPath = item.sourceIndexPath {
            
            collectionView.performBatchUpdates({
                self.FinalImagesArray.swapAt(sourceIndexPath.item,    destinationIndexPath.item)
                collectionView.reloadItems(at:   [sourceIndexPath,destinationIndexPath])
            }, completion: nil)
            coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,  dropSessionDidEnd session: UIDropSession) {
        if self.FinalImagesArray == self.InitialImagesArray    {
           //you have done it , you have made it ,you are winner of the harderst game in this world
            collectionView.dragInteractionEnabled = false
            if let vc = R.storyboard.victoryStoryboard.victoryViewController(){
                vc.isComingFromLevel = LevelType.Medium.rawValue
                self.navigationController?.pushViewController(vc, animated: false)
            }
          
        }
    }
}
