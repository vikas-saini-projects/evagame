//
//  MediumPuzzleViewController.swift
//  EvaGame
//
//  Created by Techwin on 02/03/21.
//

import UIKit
import SDWebImage

class MediumPuzzleViewController: UIViewController {
    
    @IBOutlet weak var backButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var topLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var BackgroundWhiteView: UIView!
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var viewBehindCollectionView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var solvelabel: UILabel!
    
//    var ImageAddress = "https://cdn.mos.cms.futurecdn.net/mEuBJTDhXuTfSKdLefzSKg.jpg"
    var InitialImagesArray = [UIImage]()
    var FinalImagesArray = [UIImage]()
    var ThemeID = ""
    var GameContentArray = [EasyGameModelDatum]()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        solvelabel.makeClickable(target: self, selector: #selector(speakSolve))
        setupForIphone()
        apiCallForMediumGameApi()
    }
    
    func setupForIphone(){
        if UIDevice.current.userInterfaceIdiom == .phone {
            topConstraint.constant = 10
            bottomConstraint.constant = 10
            topLabelHeight.constant = 30
            solvelabel.font = solvelabel.font.withSize(22)
            BackButton.titleLabel?.font = BackButton.titleLabel?.font.withSize(12.0)
            backButtonHeight.constant = 30
        }
    }
    
    @objc func speakSolve(){
        SpeakAText(solvelabel.text ?? "")
    }
    
    
    //MARK:- SETUP UI METHOD
    func setupUI(){
        DispatchQueue.main.async {
            self.BackgroundWhiteView.roundViewCorner(radius: 15)
            self.viewBehindCollectionView.roundViewCorner(radius: 15)
            self.BackButton.roundViewCorner(radius: self.BackButton.bounds.height / 2)
        }
    }
    
    @IBAction func GoBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


