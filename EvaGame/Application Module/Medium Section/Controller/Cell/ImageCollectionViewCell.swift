//
//  ImageCollectionViewCell.swift
//  EvaGame
//
//  Created by Techwin on 02/03/21.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var particularImage: UIImageView!
    
    override func awakeFromNib() {
        self.frame = particularImage.frame
    }
}
