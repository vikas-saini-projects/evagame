//
//  ProfileViewController.swift
//  EvaGame
//
//  Created by Techwin on 04/03/21.
//

import UIKit

class ProfileViewController: UIViewController {

    
    //MARK:- 
    @IBOutlet weak var WhiteBackgroundView: UIView!
    @IBOutlet weak var heightOFAStackElement: NSLayoutConstraint!
    @IBOutlet weak var HelloKidHeight: NSLayoutConstraint!
    @IBOutlet weak var coinsCount: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var logout: UIButton!
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var HelloLabel: UILabel!
    @IBOutlet weak var coinsStaticLabel: UILabel!
    @IBOutlet weak var staticEmailLabel: UILabel!
    @IBOutlet weak var staticNameLabel: UILabel!
    @IBOutlet weak var GapConstraint: NSLayoutConstraint!
    
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emailLabel.text = ""
        self.coinsCount.text = ""
        //commenting out this as the login and signup in not the part of app anymore
//        self.apiCallForProfileApi()
        setupButton(self.logout)
        setupButton(self.BackButton)
        self.WhiteBackgroundView.roundViewCorner(radius: 15)
        HelloLabel.makeClickable(target: self, selector: #selector(SpeakHello))
        staticNameLabel.makeClickable(target: self, selector: #selector(SpeakStaticName))
        staticEmailLabel.makeClickable(target: self, selector: #selector(SpeakStaticEmail))
        coinsStaticLabel.makeClickable(target: self, selector: #selector(SpeakStaticCoins))
        nameLabel.makeClickable(target: self, selector: #selector(SpeakName))
        emailLabel.makeClickable(target: self, selector: #selector(SpeakEmail))
        coinsCount.makeClickable(target: self, selector: #selector(SpeakBalloons))
        
        setupForIphone()
        
        //adding this as login and signup is not part of app anymore
        logout.isHidden = true
        coinsCount.text = String(UserDefaults.standard.integer(forKey: UD_COINSCOLLECTED)) + " Balloons"
        
    }
    
    
    func setupForIphone(){
        if UIDevice.current.userInterfaceIdiom == .phone {
            HelloKidHeight.constant = 50
            heightOFAStackElement.constant = 30
            HelloLabel.font = HelloLabel.font.withSize(25)
            staticNameLabel.font = staticNameLabel.font.withSize(18)
            staticEmailLabel.font = staticEmailLabel.font.withSize(18)
            coinsCount.font = coinsCount.font.withSize(24)
            coinsStaticLabel.font = HelloLabel.font.withSize(18)
            emailLabel.font = emailLabel.font.withSize(24)
            nameLabel.font = emailLabel.font.withSize(24)
            GapConstraint.constant = 10
        }
    }
    
    
    
    
    @objc func SpeakHello(){
        SpeakAText("Hello Kid")
    }
    @objc func SpeakStaticName(){
        SpeakAText("Name")
    }
    @objc func SpeakStaticEmail(){
        SpeakAText("email")
    }
    @objc func SpeakStaticCoins(){
        SpeakAText("Balloons Collected")
    }
    @objc func SpeakName(){
        SpeakAText(nameLabel.text ?? "")
    }
    @objc func SpeakEmail(){
        SpeakAText(emailLabel.text ?? "")
    }
    @objc func SpeakBalloons(){
        SpeakAText(coinsCount.text ?? "")
    }

    
    //MARK:- SETUP BUTTONS
    func setupButton(_ button : UIButton){
        DispatchQueue.main.async {
            button.layer.cornerRadius = button.bounds.height / 2
            button.layer.borderColor = UIColor.white.cgColor
            button.layer.borderWidth = 1
        }
    }
    
    //MARK:- BUTTON
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- LOGOUT BUTTON
    @IBAction func logoutButtonAction(_ sender: Any) {
        apiCallForLogoutApi()
    }
    
    
}
