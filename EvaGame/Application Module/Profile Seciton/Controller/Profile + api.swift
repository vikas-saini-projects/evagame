//
//  Profile + api.swift
//  EvaGame
//
//  Created by Techwin on 16/03/21.
//

import Foundation
import UIKit

extension ProfileViewController {
    
   //MARK:- API CALL FOR LOGOUT API
    
    func apiCallForLogoutApi(){
        startAnimating(self.view)
        let params = [String:Any]()
        ApiManager.shared.Request(type: SignupModel.self, methodType: .Get, url: BASE_URL + LOGOUT_API , parameter: params) { (error, response, message, statusCode) in
            if statusCode == 200 {
                if R.storyboard.auth.loginViewController() != nil {
                    DispatchQueue.main.async {
                    let domain = Bundle.main.bundleIdentifier!
                    UserDefaults.standard.removePersistentDomain(forName: domain)
                    UserDefaults.standard.synchronize()
                    let storyboard = R.storyboard.auth()
                    let initialViewController = storyboard.instantiateViewController(withIdentifier: R.storyboard.auth.loginViewController.identifier)
                    let nav = UINavigationController(rootViewController: initialViewController)
                    nav.isNavigationBarHidden = true
                    UIApplication.shared.windows.first?.rootViewController = nav
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                    }
                    
                }
            }else {
                if let msgStr = message {
                    Toast.show(message: msgStr, controller: self)
                }else {
                    Toast.show(message: SOMETHING_WENT_WRONG, controller: self)
                }
            }
        }
    }
    
    
    //MARK:- API CALL FOR PROFILE GET
    
    func apiCallForProfileApi(){
        let params = [String:Any]()
        ApiManager.shared.Request(type: SignupModel.self, methodType: .Get, url: BASE_URL + GET_PROFILE_API , parameter: params) { (error, response, message, statusCode) in
            if statusCode == 200 {
                if let data = response?.data {
                    PrintToConsole("response of profile api \(String(describing: response))")
                    DispatchQueue.main.async {
                    self.emailLabel.text = data.email ?? ""
                    self.coinsCount.text = data.coins ?? ""
                    }
                }else {
                    Toast.show(message: DATA_NOT_FOUND, controller: self)
                }
            }else {
                if let msgStr = message {
                    Toast.show(message: msgStr, controller: self)
                }else {
                    Toast.show(message: SOMETHING_WENT_WRONG, controller: self)
                }
            }
        }
    }
    
}
