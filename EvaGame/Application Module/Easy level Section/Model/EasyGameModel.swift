//
//  EasyGameModel.swift
//  EvaGame
//
//  Created by Techwin on 17/03/21.
//

import Foundation

// MARK: - EasyGameModel
struct EasyGameModel: Codable {
    let status: Int
    let message: String?
    let data: [EasyGameModelDatum]?
    let method, success: String?
}

// MARK: - Datum
struct EasyGameModelDatum: Codable {
    let image, sound, gameid: String?
    let name : String?
}
