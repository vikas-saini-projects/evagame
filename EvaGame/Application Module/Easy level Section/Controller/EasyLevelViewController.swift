//
//  EasyLevelViewController.swift
//  EvaGame
//
//  Created by Techwin on 04/03/21.
//

import UIKit
import AVKit

class EasyLevelViewController: UIViewController {
    
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var topLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var whiteBackgroundVIew: UIView!
    @IBOutlet weak var playLabel: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var backButtonHeight: NSLayoutConstraint!
    
    
    //MARK:- VARIABLES
    var ThemeID = ""
    var GameContentArray = [EasyGameModelDatum]()
    let ColorArray = [LIGHT_SKYBLUE_COLOR , DARK_YELLOW_COLOR , YELLOW_COLOR , PINKISH_COLOR , DARK_SKYBLUE_COLOR , DARK_BLUE_COLOR]
    var SelectedIndex : Int? = nil
    var player: AVPlayer?
    var playedIndex = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupForIphone()
        setupView()
        backButton.addTarget(self, action: #selector(GoBack), for: .touchUpInside)
        playLabel.makeClickable(target: self, selector: #selector(speakPlay))
        apiCallForEasyGameApi()
    }
    
    @objc func speakPlay(){
        SpeakAText(playLabel.text ?? "")
    }
    
    
    //MARK:- SETUP FOR IPHONE
    func setupForIphone(){
        if UIDevice.current.userInterfaceIdiom == .phone {
            topLabelConstraint.constant = 30
            playLabel.font = playLabel.font.withSize(20)
            backButtonHeight.constant = 30
            backButton.titleLabel?.font = backButton.titleLabel?.font.withSize(12)
            topConstraint.constant = 0
            bottomConstraint.constant = 0
        }
    }
    
    func setupView(){
        DispatchQueue.main.async {
            self.whiteBackgroundVIew.roundViewCorner(radius:  15)
            self.backButton.roundViewCorner(radius: self.backButton.bounds.height / 2)
            self.backButton.layer.borderWidth = 1
            self.backButton.layer.borderColor = UIColor.white.cgColor
            self.collectionView.register(UINib(nibName: R.reuseIdentifier.collectionViewCellForEasyCollection.identifier, bundle: nil), forCellWithReuseIdentifier: R.reuseIdentifier.collectionViewCellForEasyCollection.identifier)
            
        }
    }
    
    
    @objc func GoBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func playAudio(str : String){
        let playerItem = AVPlayerItem(url: URL(string: str)!)
        self.player = AVPlayer(playerItem:playerItem)
        setupAudioSession()
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        self.player?.volume = 1.0
        self.player?.play()
    }
    
    func setupAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
        } catch let error {
            debugPrint("*** Failed to activate audio session: \(error.localizedDescription)")
        }
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification) {
        guard self.player != nil else { return }
        SelectedIndex = nil
        checkForPlayedIndex()
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    
    
    
}
