//
//  Easy + api.swift
//  EvaGame
//
//  Created by Techwin on 17/03/21.
//

import Foundation
import UIKit

extension EasyLevelViewController {
    
   
    func apiCallForEasyGameApi(){
        startAnimating(self.view)
        let params = ["themeid": ThemeID , "categoryid" : CategoryID.Easy.rawValue]
        ApiManager.shared.Request(type: EasyGameModel.self, methodType: .Post, url: BASE_URL + EASY_AND_MEDIUM_GAME_API , parameter: params) { (error, response, message, statusCode) in
            if statusCode == 200 {
                if (response?.data) != nil {
                PrintToConsole("response of playGame api \(String(describing: response))")
                    DispatchQueue.main.async {
                        self.GameContentArray = response?.data ?? []
                        self.collectionView.delegate = self
                        self.collectionView.dataSource = self
                    }
               
                }else {
                    Toast.show(message: DATA_NOT_FOUND, controller: self)
                }
            }else {
                if let msgStr = message {
                    Toast.show(message: msgStr, controller: self)
                }else {
                    Toast.show(message: SOMETHING_WENT_WRONG, controller: self)
                }
            }
        }
    }
    
}


