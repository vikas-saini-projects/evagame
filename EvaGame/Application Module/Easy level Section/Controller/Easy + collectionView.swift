//
//  Easy + collectionView.swift
//  EvaGame
//
//  Created by Techwin on 04/03/21.
//

import Foundation
import UIKit
import SDWebImage

extension EasyLevelViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return GameContentArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.collectionViewCellForEasyCollection, for: indexPath) {
            let currentObject = GameContentArray[indexPath.row]
            cell.BaseView.backgroundColor = ColorArray[indexPath.row]
            cell.imageview.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            cell.imageview.sd_setImage(with: URL(string:  BASE_URL + (currentObject.image ?? "")), completed: nil)
            if SelectedIndex == indexPath.row {
                cell.speakerView.backgroundColor = GREENISH_COLOR
            }else {
                cell.speakerView.backgroundColor = DARK_SKYBLUE_COLOR
            }
            
            if UIDevice.current.userInterfaceIdiom != .pad {
                cell.heightOfSpeaker.constant = 30
                cell.speakerBottom.constant = 15
            }else {
                cell.heightOfSpeaker.constant = 45
                cell.speakerBottom.constant = 22.5
            }
          
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return CGSize(width: collectionView.bounds.width / 3, height: collectionView.bounds.height / 2)
        }else {
        return CGSize(width: collectionView.bounds.width / 3, height: collectionView.bounds.height / 2)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        SelectedIndex = indexPath.row
        if !self.playedIndex.contains(indexPath.row){
        self.playedIndex.append(indexPath.row)
        }
        self.collectionView.reloadData()
        let currentObject = GameContentArray[indexPath.row]
        SpeakAText(currentObject.name ?? "")
        playAudio(str: BASE_URL + (currentObject.sound ?? ""))
    }
    
    func checkForPlayedIndex(){
        if self.playedIndex.count == GameContentArray.count {
            //you have played all the sounds
            if let vc = R.storyboard.victoryStoryboard.victoryViewController(){
                vc.isComingFromLevel = LevelType.Easy.rawValue
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
    
}
