//
//  CollectionViewCellForEasyCollection.swift
//  EvaGame
//
//  Created by Techwin on 04/03/21.
//

import UIKit

class CollectionViewCellForEasyCollection: UICollectionViewCell {
    
    @IBOutlet weak var speakerView: UIView!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var BaseView: UIView!
    @IBOutlet weak var heightOfSpeaker: NSLayoutConstraint!
    @IBOutlet weak var speakerBottom: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        DispatchQueue.main.async {
           
           
            self.BaseView.giveShadowAndRoundCorners(shadowOffset: CGSize.zero, shadowRadius: 8, opacity: 0.8, shadowColor: .darkGray, cornerRadius: 20)
            
            self.speakerView.giveShadowAndRoundCorners(shadowOffset: CGSize.zero, shadowRadius: 4, opacity: 0.8, shadowColor: .darkGray, cornerRadius: self.speakerView.bounds.height / 2)
        }
    }
    
    

}
