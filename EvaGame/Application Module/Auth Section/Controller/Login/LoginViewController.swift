//
//  LoginViewController.swift
//  EvaGame
//
//  Created by Techwin on 01/03/21.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import AuthenticationServices

class LoginViewController: UIViewController {
    
    
    //MARK:- OUTLETS
    @IBOutlet weak var BackgroundWhiteView: UIView!
    @IBOutlet weak var SignInButton: UIButton!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfChildName: UITextField!
    @IBOutlet weak var ForgotPasswordButton: UIButton!
    @IBOutlet weak var BottomLabel: UILabel!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var BackLabel: UILabel!
    @IBOutlet weak var appleLogo: UIImageView!
    @IBOutlet weak var appleButton: UIButton!
    
    //MARK:- CLASS VARIABLES
    let loginManager = LoginManager()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        SetupUI()
        //Logging out facebook everytime when user see this screen , purely to avoid conflictions in testing
        loginManager.logOut()
        //Google Sign In
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.delegate = self
        //Logging out google everytime when user see this screen,
        if GIDSignIn.sharedInstance()?.currentUser == nil{}else {
            GIDSignIn.sharedInstance()?.signOut()
        }
        
        //make label Clickable
        welcomeLabel.makeClickable(target: self, selector: #selector(speak))
        BackLabel.makeClickable(target: self, selector: #selector(speak))
        
        
        if #available(iOS 13.0 , *) {
            appleLogo.isHidden = false
            appleButton.isHidden = false
        }
        
    }
    
    @objc func speak(){
        SpeakAText("Welcome back")
    }
    
    //MARK:- SETUP UI
    func SetupUI(){
        DispatchQueue.main.async {
            self.BackgroundWhiteView.layer.cornerRadius = 20
            self.SignInButton.layer.borderWidth = 1
            self.SignInButton.layer.borderColor = UIColor.white.cgColor
            self.SignInButton.layer.cornerRadius = 20
            self.tfChildName.placeholderColor(color: UIColor.black)
            self.tfPassword.placeholderColor(color: UIColor.black)
            self.AttibutedTextToLabel()
            self.ForgotPasswordButton.addTarget(self, action: #selector(self.ForgotPasswordAction), for: UIControl.Event.touchUpInside)
        }
    }
    
    //MARK:- ATTRIBUTED LABEL TEXT
    func AttibutedTextToLabel(){
        let textAttributesOne = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "WickedMouse", size: 16.0)!]
        let textAttributesTwo = [NSAttributedString.Key.foregroundColor: DARK_BLUE_COLOR, NSAttributedString.Key.font: UIFont(name: "WickedMouse", size: 16.0)! , NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue , NSAttributedString.Key.underlineColor : UIColor.black] as [NSAttributedString.Key : Any]
        let textPartOne = NSMutableAttributedString(string: "DON'T HAVE AN ACCOUNT? ", attributes: textAttributesOne)
        let textPartTwo = NSMutableAttributedString(string: "SIGN UP", attributes: textAttributesTwo)
        let textCombination = NSMutableAttributedString()
        textCombination.append(textPartOne)
        textCombination.append(textPartTwo)
        self.BottomLabel.attributedText = textCombination
        //tap action
        self.BottomLabel.makeClickable(target: self, selector: #selector(TapOnSignUp))
        
    }
    
    
    //MARK: BUTTON ACTIONS
    @IBAction func ActionOnSignInButton(_ sender: Any) {
        let emailString = tfChildName.text?.trimmed() ?? ""
        let passwordString = tfPassword.text?.trimmed() ?? ""
        let confirmPasswordString = tfPassword.text?.trimmed() ?? ""
        do {
            let signUpData = try SignUpData(with: emailString, password: passwordString, confirmPassword: confirmPasswordString)
            apiCallForLoginApi(signUpData.email, password:signUpData.password, loginType:LoginType.normal.rawValue , social_id: nil)
        } catch let error as ValidationError {
            Toast.show(message: error.message, controller: self)
        } catch {}
    }
    
    @IBAction func ActionForGoogleButton(_ sender: Any) {
        if GIDSignIn.sharedInstance()?.currentUser == nil {
            GIDSignIn.sharedInstance().signIn()
        }
        else {
            GIDSignIn.sharedInstance()?.signOut()
        }
    }
    
    @IBAction func ActionOnFacebookButton(_ sender: Any) {
        self.facebookLogin()
    }
    
    @IBAction func ActionForAppleButton(_ sender: Any) {
        if #available(iOS 13.0, *) {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
        }
    }
    
    @objc func TapOnSignUp(){
        if let vc = R.storyboard.auth.signupViewController() {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func ForgotPasswordAction(){
        if let vc = R.storyboard.auth.forgotPasswordViewController() {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
