//
//  Login + Facebook.swift
//  EvaGame
//
//  Created by Techwin on 04/03/21.
//

import Foundation
import FBSDKLoginKit
import FBSDKCoreKit

extension LoginViewController {
    
    
    //MARK:- FACEBOOK LOGIN FUNCTION
    func facebookLogin(){
        if(AccessToken.current == nil ) {
            loginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
                if ((result?.isCancelled)!) {
                }
                else {
                    if (error == nil) {
                        let params = ["fields" : "id, name, first_name, last_name, picture.type(large), email "]
                        let graphRequest = GraphRequest.init(graphPath: "/me", parameters: params)
                        let Connection = GraphRequestConnection()
                        Connection.add(graphRequest) { (Connection, result, error) in
                            let info = result as! [String : AnyObject]
                            let picDict = info["picture"] as? [String : Any ]
                            let dataDict = picDict!["data"] as! [String:Any]
                            
                            let _ = dataDict["url"] as? String
                            let _ = info["first_name"] as? String ?? ""
                            let _ = info["last_name"] as? String ?? ""
                            
                            let socialID = info["id"] as? String ?? ""
                            let email = info["email"] as? String ?? ""

                            
                            let emailString = email
                            let passwordString = "123456"
                            let confirmPasswordString = "123456"
                            do {
                                let signUpData = try SignUpData(with: emailString, password: passwordString, confirmPassword: confirmPasswordString)
                                self.apiCallForLoginApi(signUpData.email, password:nil, loginType:LoginType.social.rawValue , social_id: socialID)
                            } catch let error as ValidationError {
                                Toast.show(message: error.message, controller: self)
                            } catch {}
                            
                            
                        }
                        Connection.start()
                    }
                }
                if let error = error {
                    let alertController = UIAlertController(title: "Message", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                    return
                }
            }
        }else {
            loginManager.logOut()
            let alertController = UIAlertController(title: "Message", message: "Logged Out", preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
