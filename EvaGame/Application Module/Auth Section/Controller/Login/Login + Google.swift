//
//  Login + Google.swift
//  EvaGame
//
//  Created by Techwin on 04/03/21.
//

import Foundation
import GoogleSignIn


extension LoginViewController : GIDSignInDelegate {
    
    //MARK:- GOOGLE SIGN IN
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            if GIDSignIn.sharedInstance()?.currentUser != nil{
                let _ = user.profile.name ?? "--"
                let email = user.profile.email!
                let socialID = user.userID!
                if  user.profile.hasImage {}
                
                let emailString = email
                let passwordString = "123456"
                let confirmPasswordString = "123456"
                do {
                    let signUpData = try SignUpData(with: emailString, password: passwordString, confirmPassword: confirmPasswordString)
                    self.apiCallForLoginApi(signUpData.email, password:nil, loginType:LoginType.social.rawValue , social_id: socialID)
                } catch let error as ValidationError {
                    Toast.show(message: error.message, controller: self)
                } catch {}
                
            }
            else{
                Toast.show(message: "Error While Google Sign in", controller: self, color: .red)
            }
        }
        else {
            print("\(error.localizedDescription)")
        }
    }
    
}
