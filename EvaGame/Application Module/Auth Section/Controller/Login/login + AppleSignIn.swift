//
//  login + AppleSignIn.swift
//  EvaGame
//
//  Created by Techwin on 15/03/21.
//

import Foundation
import AuthenticationServices


@available(iOS 13.0, *)
extension LoginViewController : ASAuthorizationControllerDelegate {
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            
            let userIdentifier = appleIDCredential.user
            let email = appleIDCredential.email
            
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            appleIDProvider.getCredentialState(forUserID: userIdentifier) {  (credentialState, error) in
                switch credentialState {
                case .authorized:
                    // The Apple ID credential is valid.
                    if email != nil {
                    self.apiCallForLoginApi(email ?? "", password: nil, loginType: LoginType.social.rawValue, social_id: userIdentifier )
                    }else {
                        Toast.show(message: "No Email Address Found !", controller: self)
                    }
                    break
                case .revoked:
                    // The Apple ID credential is revoked.
                    Toast.show(message: "The Apple ID credential is revoked.", controller: self)
                    break
                case .notFound:
                    // No credential was found, so show the sign-in UI.
                    Toast.show(message: "No credential was found", controller: self)
                    
                    break
                default:
                    break
                }
            }
        }
    }
    
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        PrintToConsole("Error while apple sign in \(error.localizedDescription)")
        Toast.show(message: "Error : \(error.localizedDescription ?? "")", controller: self)
    }
    
    
}
