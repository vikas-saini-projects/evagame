//
//  SignupViewController.swift
//  EvaGame
//
//  Created by Techwin on 01/03/21.
//

import UIKit

class SignupViewController: UIViewController {
    
    @IBOutlet weak var backgroundWhiteView: UIView!
    @IBOutlet weak var tfChildName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfAge: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var SigninButton: UIButton!
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var joinUsLabel: UILabel!
    
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        SetupUI()
        welcomeLabel.makeClickable(target: self, selector: #selector(SpeakWelcom))
        joinUsLabel.makeClickable(target: self, selector: #selector(SpeakJoinUs))
    }
    
    @objc func SpeakWelcom(){
        SpeakAText("Welcome")
    }
    
    @objc func SpeakJoinUs(){
        SpeakAText("Join Us")
    }
    
    
    //MARK:- SETUP UI
    func SetupUI(){
        DispatchQueue.main.async {
            self.backgroundWhiteView.layer.cornerRadius = 20
            self.SigninButton.layer.borderWidth = 1
            self.SigninButton.layer.borderColor = UIColor.white.cgColor
            self.SigninButton.layer.cornerRadius = 20
            self.tfChildName.placeholderColor(color: UIColor.black)
            self.tfPassword.placeholderColor(color: UIColor.black)
            self.tfAge.placeholderColor(color: UIColor.black)
            self.tfEmail.placeholderColor(color: UIColor.black)
            self.AttibutedTextToLabel()
        }
    }
    
    //MARK:- ATTRIBUTED LABEL TEXT
    func AttibutedTextToLabel(){
        let textAttributesOne = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "WickedMouse", size: 16.0)!]
        let textAttributesTwo = [NSAttributedString.Key.foregroundColor: DARK_BLUE_COLOR, NSAttributedString.Key.font: UIFont(name: "WickedMouse", size: 16.0)! , NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue , NSAttributedString.Key.underlineColor : UIColor.black] as [NSAttributedString.Key : Any]
        let textPartOne = NSMutableAttributedString(string: "ALREADY HAVE AN ACCOUNT? ", attributes: textAttributesOne)
        let textPartTwo = NSMutableAttributedString(string: "SIGN IN", attributes: textAttributesTwo)
        let textCombination = NSMutableAttributedString()
        textCombination.append(textPartOne)
        textCombination.append(textPartTwo)
        self.bottomLabel.attributedText = textCombination
        //tap action
      
        self.bottomLabel.makeClickable(target: self, selector: #selector(TapOnSignIN))
    }
    
    //MARK:- ACTION ON SIGNUP BUTTON
    @IBAction func ActionOnSignupButton(_ sender: Any) {
        let emailString = tfEmail.text?.trimmed() ?? ""
        let passwordString = tfPassword.text?.trimmed() ?? ""
        let confirmPasswordString = tfPassword.text?.trimmed() ?? ""
        do {
            let signUpData = try SignUpData(with: emailString, password: passwordString, confirmPassword: confirmPasswordString)
            apiCallForSignUpApi(signUpData.email, password: passwordString)
        } catch let error as ValidationError {
            Toast.show(message: error.message, controller: self)
        } catch {}
    }
    
    //MARK:- TAP ON SIGN IN
    @objc func TapOnSignIN(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}
