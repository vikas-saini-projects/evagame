//
//  Signup + api.swift
//  EvaGame
//
//  Created by Techwin on 16/03/21.
//

import Foundation
import UIKit

extension SignupViewController {
    
   
    func apiCallForSignUpApi(_ email : String , password : String){
        startAnimating(self.view)
        let params = ["email": email , "password" : password , "devicetype" : DEVICE_TYPE , "devicetoken": DEVICE_TOKKEN]
        ApiManager.shared.Request(type: SignupModel.self, methodType: .Post, url: BASE_URL + SIGNUP_API , parameter: params) { (error, response, message, statusCode) in
            if statusCode == 200 {
                if (response?.data) != nil {
                    PrintToConsole("response of signup api \(String(describing: response))")
                UserDefaults.standard.set(response?.data?.email ?? "", forKey: UD_EMAIL)
                UserDefaults.standard.set(response?.data?.sessionkey ?? "", forKey: UD_SESSIONKEY)
                UserDefaults.standard.set(response?.data?.userid ?? "", forKey: UD_USERID)
                DispatchQueue.main.async {
                if let vc = R.storyboard.chooseTheme.chooseThemeViewController() {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                }
                }else {
                    Toast.show(message: DATA_NOT_FOUND, controller: self)
                }
            }else {
                if let msgStr = message {
                    Toast.show(message: msgStr, controller: self)
                }else {
                    Toast.show(message: SOMETHING_WENT_WRONG, controller: self)
                }
            }
        }
    }
    
}
