//
//  ForgotPassword + api.swift
//  EvaGame
//
//  Created by Techwin on 17/03/21.
//

import Foundation
import UIKit

extension ForgotPasswordViewController {
    
   
    func apiCallForForgotPasswordApi(_ email : String){
        startAnimating(self.view)
        let params = ["email": email]
        ApiManager.shared.Request(type: SignupModel.self, methodType: .Post, url: BASE_URL + FORGOT_PASSWORD_API , parameter: params) { (error, response, message, statusCode) in
            if statusCode == 200 {
                Toast.show(message: "New password is sent to your registered Email address.", controller: self)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.navigationController?.popViewController(animated: true)
                }
                
            }else {
                if let msgStr = message {
                    Toast.show(message: msgStr, controller: self)
                }else {
                    Toast.show(message: SOMETHING_WENT_WRONG, controller: self)
                }
            }
        }
    }
    
}
