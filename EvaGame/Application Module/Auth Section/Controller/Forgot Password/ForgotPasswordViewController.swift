//
//  ForgotPasswordViewController.swift
//  EvaGame
//
//  Created by Admin on 01/03/21.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var TfEmai: UITextField!
    @IBOutlet weak var backGroundWhiteView: UIView!
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var resetPasswordView: UIButton!
    @IBOutlet weak var ForgotPasswordLabel: UILabel!
    @IBOutlet weak var weJustNeedLabel: UILabel!
    
    
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        weJustNeedLabel.makeClickable(target: self, selector: #selector(SpeakWeJustNeed))
        ForgotPasswordLabel.makeClickable(target: self, selector: #selector(SpeakForgot))
    }
    
    @objc func SpeakWeJustNeed(){
        SpeakAText(weJustNeedLabel.text!)
    }
    @objc func SpeakForgot(){
        SpeakAText(ForgotPasswordLabel.text ?? "")
    }
    
    //MARK:- SETUP UI
    func setupUI(){
        DispatchQueue.main.async {
            self.backGroundWhiteView.layer.cornerRadius = 20
            self.resetPasswordView.layer.borderWidth = 1
            self.resetPasswordView.layer.borderColor = UIColor.white.cgColor
            self.resetPasswordView.layer.cornerRadius = 20
            self.BackButton.layer.borderWidth = 1
            self.BackButton.layer.borderColor = UIColor.white.cgColor
            self.BackButton.layer.cornerRadius = 20
            self.TfEmai.placeholderColor(color: UIColor.black)
            
        }
    }
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func resetPasswordButton(_ sender: Any) {
        let emailString = TfEmai.text?.trimmed() ?? ""
        let passwordString = "123456"
        let confirmPasswordString = "123456"
        do {
            let signUpData = try SignUpData(with: emailString, password: passwordString, confirmPassword: confirmPasswordString)
            self.apiCallForForgotPasswordApi(signUpData.email)
        } catch let error as ValidationError {
            Toast.show(message: error.message, controller: self)
        } catch {}
    }
    
    @IBAction func BackButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
