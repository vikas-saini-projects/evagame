//
//  ChooseThemeModel.swift
//  EvaGame
//
//  Created by Techwin on 17/03/21.
//

import Foundation

// MARK: - GetThemesModel
struct GetThemesModel: Codable {
    let status: Int
    let message: String?
    let data: [GetThemesModelDataClass]?
    let method, success: String
}

// MARK: - GetThemesModelDataClass
struct GetThemesModelDataClass: Codable {
    let theme_id, name, image, isactive: String?
    let created_on: String?
}
