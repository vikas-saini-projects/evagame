//
//  ChooseThemeViewController.swift
//  EvaGame
//
//  Created by Admin on 01/03/21.
//

import UIKit

class ChooseThemeViewController: UIViewController {

    
    //MARK:- OUTLETS
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var ProfileButton: UIButton!
    @IBOutlet weak var backgroundWhiteView: UIView!
    @IBOutlet weak var chooseThemeLabel: UILabel!
    @IBOutlet weak var heightOfButton: NSLayoutConstraint!
    
    //MARK:- CLASS CONSTANTS AND VARIABLES
    var SelectedIndex : Int? = nil
//    var ImagesNames = ["Untitled-19","Untitled-20","Untitled-21","Untitled-22","Untitled-23","Untitled-24"]
//    var Strings = ["JUNGLE ANIMALS","FARM ANIMALS" , "AFRICAN ANIMALS", "PETS" , "CLOTHERS" , "EMOTIONS"]
    var allThemesData = [GetThemesModelDataClass]()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        chooseThemeLabel.makeClickable(target: self, selector: #selector(speakChooseTheme))
        setupForIphone()
        apiCallForGrtThemesApi()
    }
    
    @objc func speakChooseTheme(){
        SpeakAText(chooseThemeLabel.text ?? "")
    }
    
    func setupForIphone(){
        if UIDevice.current.userInterfaceIdiom == .phone {
            heightOfButton.constant = 30
        }
    }
    
    //MARK:- ACTIONS
    func setupUI(){
        DispatchQueue.main.async {
            self.collectionview.register(UINib(nibName: R.reuseIdentifier.collectionViewCellForChooseTheme.identifier, bundle: nil), forCellWithReuseIdentifier: R.reuseIdentifier.collectionViewCellForChooseTheme.identifier)
            self.backgroundWhiteView.layer.cornerRadius = 20
            self.setupButton(self.ProfileButton)
        }
    }

    //MARK:- SETUP BUTTONS
    func setupButton(_ button : UIButton){
        DispatchQueue.main.async {
            button.layer.cornerRadius = button.bounds.height / 2
            button.layer.borderColor = UIColor.white.cgColor
            button.layer.borderWidth = 1
        }
    }
    
    //MARK:- PROFILE BUTTON ACTION
    @IBAction func profileButtonAction(_ sender: Any) {
        if let vc =  R.storyboard.profile.profileViewController() {
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
