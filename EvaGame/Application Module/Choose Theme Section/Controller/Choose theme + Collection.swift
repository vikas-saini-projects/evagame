//
//  Choose theme + Collection.swift
//  EvaGame
//
//  Created by Admin on 01/03/21.
//

import Foundation
import UIKit
import SDWebImage

//MARK:-  EXTENSION FOR COLLECTIION VIEW

extension ChooseThemeViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    //MARK:- NUMBER OF ITEMS
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allThemesData.count
    }
    
    
    //MARK:- COLLECITON VIEW CELL
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.collectionViewCellForChooseTheme.identifier, for: indexPath) as? CollectionViewCellForChooseTheme {
            cell.imagaeView.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            cell.imagaeView.sd_setImage(with: URL(string: BASE_URL + (allThemesData[indexPath.row].image ?? "")), completed: nil)
            cell.labelForText.text = allThemesData[indexPath.row].name
            cell.PlayNowButton.tag = indexPath.row
            cell.PlayNowButton.addTarget(self, action: #selector(PlayNowTapped), for: UIControl.Event.touchUpInside)
            if SelectedIndex == indexPath.row {
                cell.PlayNowButton.backgroundColor = R.color.greenisH()
            }else {
                cell.PlayNowButton.backgroundColor = R.color.dark_SKY_BLUE()
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    
    //MARK:- DID SELECT ITEM AT FOR COLLECTIONVIEW
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        SelectedIndex = indexPath.row
        DispatchQueue.main.async {
            self.collectionview.reloadData()
        }
    }
    
    //MARK:- PLAY NOW TAPPED
    @objc func PlayNowTapped(_ sender : UIButton){
        if SelectedIndex != sender.tag {
            SelectedIndex = sender.tag
            DispatchQueue.main.async {
                self.collectionview.reloadData()
            }
        }else {
            if let vc = R.storyboard.chooseLevel.levelViewController() {
                vc.ThemeID = allThemesData[sender.tag].theme_id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    //MARK:- COLLECTION VIEW ITEM SIZE
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            let width = (collectionView.bounds.width - 40 ) / 3 //because spacing is 20 and three items space so 20*2
            let height : CGFloat = 220.0 //because spacing is 20 and two items have 1 space so 20
            return CGSize(width: width, height: height)
        }else {
            let width = (collectionView.bounds.width - 40 ) / 3 //because spacing is 20 and three items space so 20*2
            let height = (collectionView.bounds.height - 20 ) / 2 //because spacing is 20 and two items have 1 space so 20
            return CGSize(width: width, height: height)
        }
    }
    
}
