//
//  ChooseTheme + api.swift
//  EvaGame
//
//  Created by Techwin on 17/03/21.
//

import Foundation
import UIKit

extension ChooseThemeViewController {
    
    //MARK:- API CALL FOR GET THEMES
    
    func apiCallForGrtThemesApi(){
        startAnimating(self.view)
        let params = [String:Any]()
        ApiManager.shared.Request(type: GetThemesModel.self, methodType: .Get, url: BASE_URL + GET_ALL_THEMES_API , parameter: params) { (error, response, message, statusCode) in
            if statusCode == 200 {
                if let data = response?.data {
                    PrintToConsole("response of getThemes api \(String(describing: response))")
                    DispatchQueue.main.async {
                        self.allThemesData = data
                        self.collectionview.delegate = self
                        self.collectionview.dataSource = self
                    }
                }else {
                    Toast.show(message: DATA_NOT_FOUND, controller: self)
                }
            }else {
                if let msgStr = message {
                    Toast.show(message: msgStr, controller: self)
                }else {
                    Toast.show(message: SOMETHING_WENT_WRONG, controller: self)
                }
            }
        }
    }
}
