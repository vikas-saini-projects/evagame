//
//  CollectionViewCellForChooseTheme.swift
//  EvaGame
//
//  Created by Admin on 01/03/21.
//

import UIKit

class CollectionViewCellForChooseTheme: UICollectionViewCell {

    @IBOutlet weak var BaseView: UIView!
    @IBOutlet weak var imagaeView: UIImageView!
    @IBOutlet weak var PlayNowButton: UIButton!
    @IBOutlet weak var labelForText: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        BaseView.layer.cornerRadius = 15
        PlayNowButton.layer.cornerRadius = 15
        if UIDevice.current.userInterfaceIdiom == .pad {
            labelForText.font = labelForText.font.withSize(16)
        }else {
            labelForText.font = labelForText.font.withSize(10)
        }
    }

}
