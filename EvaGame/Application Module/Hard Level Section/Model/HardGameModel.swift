//
//  HardGameModel.swift
//  EvaGame
//
//  Created by Techwin on 17/03/21.
//

import Foundation


// MARK: - HardGameModel
struct HardGameModel: Codable {
    let status: Int
    let message: String?
    let data: HardGameModelDataClass?
    let method, success: String?
}

// MARK: - DataClass
struct HardGameModelDataClass: Codable {
    let leftside: [Leftside]?
    let rightside: [Rightside]?
}

// MARK: - Leftside
struct Leftside: Codable {
    let image, gameid: String
}

// MARK: - Rightside
struct Rightside: Codable {
    let name, gameid: String
}

