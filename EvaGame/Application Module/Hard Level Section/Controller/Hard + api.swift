//
//  Hard + api.swift
//  EvaGame
//
//  Created by Techwin on 17/03/21.
//

import Foundation
import UIKit
import SDWebImage

extension HardLevelViewController {
    
    func apiCallForHardGameApi(){
        startAnimating(self.view)
        let params = ["themeid": ThemeID , "categoryid" : CategoryID.Hard.rawValue]
        ApiManager.shared.Request(type: HardGameModel.self, methodType: .Post, url: BASE_URL + HARD_GAME_API , parameter: params) { (error, response, message, statusCode) in
            if statusCode == 200 {
                if (response?.data) != nil {
                PrintToConsole("response of Hard Game api \(String(describing: response))")
                    DispatchQueue.main.async {
                        if let ImagesObjectArray = response?.data?.leftside , let _ = response?.data?.rightside {
                            for obj in ImagesObjectArray {
                                self.RightMatches[Int(obj.gameid)!] = Int(obj.gameid)!
                            }
                        print("rightMatches \(self.RightMatches)")
                        self.HardGameData = response?.data
                        self.collectionViewImage.delegate = self
                        self.collectionViewImage.dataSource = self
                        self.collectionViewWords.dataSource = self
                        self.collectionViewWords.delegate = self
                        }
                    }
                }else {
                    Toast.show(message: DATA_NOT_FOUND, controller: self)
                }
            }else {
                if let msgStr = message {
                    Toast.show(message: msgStr, controller: self)
                }else {
                    Toast.show(message: SOMETHING_WENT_WRONG, controller: self)
                }
            }
        }
    }
    
}
