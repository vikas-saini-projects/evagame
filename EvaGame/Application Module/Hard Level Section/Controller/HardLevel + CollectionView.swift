//
//  HardLevel + CollectionView.swift
//  EvaGame
//
//  Created by Admin on 03/03/21.
//

import Foundation
import UIKit
import SDWebImage

extension HardLevelViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return HardGameData?.leftside?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewImage {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.collectionViewCellForHardImage.identifier, for: indexPath) as? CollectionViewCellForHardImage {
                if let array = HardGameData?.leftside {
                    cell.imageview.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                    cell.imageview.sd_setImage(with: URL(string: BASE_URL + array[indexPath.row].image), completed: nil)
                    cell.BaseView.backgroundColor = ColorArray[indexPath.row]
                }
                return cell
            }
            return UICollectionViewCell()
        }else {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.collectionViewCellForHardText.identifier, for: indexPath) as? CollectionViewCellForHardText {
                if let array = HardGameData?.rightside {
                    cell.labelForText.text = array[indexPath.row].name
                }
                if UIDevice.current.userInterfaceIdiom == .phone {
                    cell.labelForText.font = cell.labelForText.font.withSize(09)
                }else {
                    cell.labelForText.font = cell.labelForText.font.withSize(20)
                }
                return cell
            }
            return UICollectionViewCell()
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.height / 4, height: collectionView.bounds.height / 4)
    }
    
    
    
    
}
