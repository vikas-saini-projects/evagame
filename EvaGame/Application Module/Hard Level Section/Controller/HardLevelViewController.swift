//
//  HardLevelViewController.swift
//  EvaGame
//
//  Created by Techwin on 02/03/21.
//

import UIKit

class HardLevelViewController: UIViewController {

   //MARK:- OUTLETS
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var GapConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightOfTopLabel: NSLayoutConstraint!
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    @IBOutlet weak var topHeight: NSLayoutConstraint!
    @IBOutlet weak var BackgroundWhiteView: UIView!
    @IBOutlet weak var collectionViewImage: UICollectionView!
    @IBOutlet weak var collectionViewWords: UICollectionView!
    @IBOutlet weak var viewContainingCollectionViews: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var matchLabel: UILabel!
    @IBOutlet weak var BackButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    
    
//    var ImagesArray = [#imageLiteral(resourceName: "lion"),#imageLiteral(resourceName: "cow"),#imageLiteral(resourceName: "Dog"),#imageLiteral(resourceName: "Elephent")]
//    var TextArray = ["Cow", "Dog" , "Elephant","lion"]
    let ColorArray = [LIGHT_SKYBLUE_COLOR , DARK_YELLOW_COLOR , YELLOW_COLOR , PINKISH_COLOR]
    var startingPoint : CGPoint? = nil
    var IndexForFirst : Int?
    var matches = [Int:Int]()
    var RightMatches = [Int:Int]()
    
    
    var ThemeID = ""
    var HardGameData : HardGameModelDataClass?
  
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
         super.viewDidLoad()
        setupView()
        matchLabel.makeClickable(target: self, selector: #selector(speakMatch))
        setupForIphone()
        apiCallForHardGameApi()
    }
    
    func setupForIphone(){
        if UIDevice.current.userInterfaceIdiom == .phone {
        heightOfTopLabel.constant = 30
        matchLabel.font = matchLabel.font.withSize(20)
        BackButtonHeight.constant = 30
        topHeight.constant = 20
        bottomHeight.constant = 20
        GapConstraint.constant = 10
        stackView.spacing = UIScreen.main.bounds.width * 0.2
        leadingConstraint.constant = 120
        trailingConstraint.constant = 120
        }
    }
   
    
    @objc func speakMatch(){
        SpeakAText("match the followings")
    }
    
    //MARK:- TOUCH BEGAN
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: self.collectionViewImage)
            PrintToConsole("start Position \(position)")
            let point = stackView.convert(position , to: stackView)
            PrintToConsole("End Position \(point)")
            if let indexPath = self.collectionViewImage.indexPathForItem(at: position) {
                self.startingPoint = point
                if let leftSideArray = HardGameData?.leftside {
                    self.IndexForFirst = Int(leftSideArray[indexPath.row].gameid) ?? 0// indexPath.row
                }
            }
            }
    }
    
    //MARK:- TOUCH ENDED
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let x = self.collectionViewWords.frame.origin.x
            let position = touch.location(in: self.collectionViewWords )
            var point = stackView.convert(position , to: stackView)
            point.x = point.x + x
            // In Above logic we get the touch position in collectionViewWords and then convert that postion in stackView , then add origin x to get required position
            if let indexPath = self.collectionViewWords.indexPathForItem(at: position){
                PrintToConsole("indexPath ending \(indexPath)")
                if let StartedFrom = startingPoint , let indexFirst = IndexForFirst{
                    if !matches.keys.contains(indexFirst) {
                        if let RightSideArray = HardGameData?.rightside {
                            matches[indexFirst] = Int(RightSideArray[indexPath.row].gameid) ?? 0
                        }
                       
                        DispatchQueue.main.async {
                            drawLineFromPoint(start: StartedFrom, toPoint: point, ofColor: GREENISH_COLOR, inView: self.stackView , tag: "\(indexFirst)")
                        }
                        startingPoint = nil
                        IndexForFirst = nil
                    }else {
                        //if it is matched before , we need to remove that match
                        matches.removeValue(forKey: indexFirst)
                        for sublayer in self.stackView.layer.sublayers! {
                            if sublayer.name == "\(indexFirst)"
                            {
                                sublayer.removeFromSuperlayer()
                            }
                        }//end of the loop
                        if let RightSideArray = HardGameData?.rightside {
                            matches[indexFirst] = Int(RightSideArray[indexPath.row].gameid) ?? 0
                        }
                        DispatchQueue.main.async {
                            drawLineFromPoint(start: StartedFrom, toPoint: point, ofColor: GREENISH_COLOR, inView: self.stackView , tag: "\(indexFirst)")
                        }
                        startingPoint = nil
                        IndexForFirst = nil
                    }
                    //checking if all the matches are made
                    if matches.count == 4 {
                        if matches == RightMatches {
                            PrintToConsole("Note It Down , you are right")
                            if let vc = R.storyboard.victoryStoryboard.victoryViewController(){
                                vc.isComingFromLevel = LevelType.Hard.rawValue
                                self.navigationController?.pushViewController(vc, animated: false)
                            }
                        }
                    }//== end of the check
                }
            }
        }
    }
    
    
    //MARK:- SETUP VIEW
    func setupView(){
        DispatchQueue.main.async {
            self.backButton.roundViewCorner(radius: self.backButton.bounds.height / 2)
            self.BackgroundWhiteView.roundViewCorner(radius: 15)
        }
        self.collectionViewWords.register(UINib(nibName: R.reuseIdentifier.collectionViewCellForHardText.identifier, bundle: nil), forCellWithReuseIdentifier: R.reuseIdentifier.collectionViewCellForHardText.identifier)
        self.collectionViewImage.register(UINib(nibName: R.reuseIdentifier.collectionViewCellForHardImage.identifier, bundle: nil), forCellWithReuseIdentifier: R.reuseIdentifier.collectionViewCellForHardImage.identifier)
        self.collectionViewWords.backgroundColor = .clear
        self.collectionViewImage.backgroundColor = .clear
    }
    
    
    //MARK:- BACK BUTTON ACTION
    @IBAction func BackButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}


