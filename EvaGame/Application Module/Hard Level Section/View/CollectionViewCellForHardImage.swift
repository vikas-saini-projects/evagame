//
//  CollectionViewCellForHardImage.swift
//  EvaGame
//
//  Created by Admin on 03/03/21.
//

import UIKit

class CollectionViewCellForHardImage: UICollectionViewCell {

    @IBOutlet weak var BaseView: UIView!
    @IBOutlet weak var imageview: UIImageView!
   
    @IBOutlet weak var equalWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var equalHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.BaseView.giveShadowAndRoundCorners(shadowOffset: .zero, shadowRadius: 08, opacity: 0.8, shadowColor: UIColor.darkGray, cornerRadius: self.BaseView.bounds.height / 2)
            if UIDevice.current.userInterfaceIdiom == .phone {
                self.equalWidthConstraint.constant = 0.9
                self.equalHeightConstraint.constant = 0.9
            }else {
                self.equalWidthConstraint.constant = 0.8
                self.equalHeightConstraint.constant = 0.7
            }
        }
    }

}
