//
//  ColorManager.swift
//  EvaGame
//
//  Created by Techwin on 01/03/21.
//

import Foundation

import UIKit

let DARK_BLUE_COLOR = UIColor(displayP3Red: 30/255, green: 42/255, blue: 141/255, alpha: 1.0)

let DARK_SKYBLUE_COLOR = UIColor(displayP3Red: 130/255, green: 234/255, blue: 242/255, alpha: 1.0)

let YELLOW_COLOR = UIColor(displayP3Red: 252/255, green: 246/255, blue: 194/255, alpha: 1.0)

let GREENISH_COLOR = UIColor(displayP3Red: 132/255, green: 227/255, blue: 112/255, alpha: 1.0)

let PINKISH_COLOR = UIColor(displayP3Red: 249/255, green: 216/255, blue: 228/255, alpha: 1.0)

let LIGHT_SKYBLUE_COLOR = UIColor(displayP3Red: 173/255, green: 226/255, blue: 212/255, alpha: 1.0)

let DARK_YELLOW_COLOR = UIColor(displayP3Red: 243/255, green: 244/255, blue: 149/255, alpha: 1.0)
