//
//  Enums.swift
//  DummyProject
//
//  Created by Vikas saini on 26/01/21.
//

import Foundation

//MARK:- ENUM FOR METHOD TYPE
enum MethodType : String {
    case Post = "POST"
    case Get = "GET"
    case Put = "PUT"
    case Patch = "PATCH"
    case Delete = "DELETE"
}


//MARK:- ENUM FOR LEVEL

enum LevelType: Int {
    case Easy = 0
    case Hard = 2
    case Medium = 1
}



//MARK:- LOGIN TYPE

enum LoginType: Int {
    case normal = 1
    case social = 2
}


//MARK:- CATEGORY FOR LEVEL IN API

enum CategoryID : String {
    case Easy = "1"
    case Hard = "3"
    case Medium = "2"
}
