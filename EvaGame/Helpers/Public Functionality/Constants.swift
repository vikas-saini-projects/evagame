//
//  Constants.swift
//  EvaGame
//
//  Created by Techwin on 16/03/21.
//

import Foundation


let BASE_URL = "http://techwinlabs.in/evagame/"


let SOMETHING_WENT_WRONG = "Something went wrong!"
let DATA_NOT_FOUND = "No Data Found !"



let UD_USERID = "UDUSERID"
let UD_SESSIONKEY = "UDSESSIONKEY"
let UD_EMAIL = "UDEMAIL"
var DEVICE_TYPE = "1"
var DEVICE_TOKKEN = "123456897"
let UD_COINSCOLLECTED = "UD_COINSCOLLECTED"


let SIGNUP_API = "Auth/signup"
let LOGIN_API = "Auth/login"
let LOGOUT_API = "Auth/logout"
let GET_PROFILE_API = "Auth/getProfile"
let GET_ALL_THEMES_API = "Auth/get_themes"
let EASY_AND_MEDIUM_GAME_API = "Auth/playGame"
let COINS_API = "Auth/getReward"
let HARD_GAME_API = "Auth/thirdLevel"
let FORGOT_PASSWORD_API = "Auth/forgot_password"


