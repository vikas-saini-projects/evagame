//
//  PublicFunctions.swift
//  DummyProject
//
//  Created by Vikas saini on 16/01/21.
//

import Foundation
import AVFoundation
import UIKit

//MARK:- PRINT TO CONSOLE

//CONTRIBUTED BY VIKAS SAINI DATED 16 JAN 2021
//FUNCTION USE PRINT STATEMENT TO PRINT ONLY IN DEBUG MODE AND PREVENT TO  PRINT TO CONSOLE IF IS NOT DEBUGGING
//PRINT STATEMENT IN RELEASE MODE CAN SLOW DOWN THE APPLICATION

public func PrintToConsole(_ message: String) {
    #if DEBUG
    print(message)
    #endif
}

//USAGE :  PrintToConsole("SomeMessage")

//MARK:- DRAW LINE FROM ONE POINT TO ANOTHER POINT
func drawLineFromPoint(start : CGPoint, toPoint end:CGPoint, ofColor lineColor: UIColor, inView view:UIView , tag:String) {
    //design the path
    let path = UIBezierPath()
    path.move(to: start)
    path.addLine(to: end)
    //design path in layer
    let shapeLayer = CAShapeLayer()
    shapeLayer.path = path.cgPath
    shapeLayer.strokeColor = lineColor.cgColor
    shapeLayer.lineWidth = 4.0
    shapeLayer.name = tag
    view.layer.addSublayer(shapeLayer)
}

//MARK:- COMPARE TWO DICTIONARIES
public func ==(lhs: [String: AnyObject], rhs: [String: AnyObject] ) -> Bool {
    return NSDictionary(dictionary: lhs).isEqual(to: rhs)
}



//MARK:- SLICE AN IMAGE INTO MANY PARTS
func slice(image: UIImage, into howMany: Int) -> [UIImage] {
    let width: CGFloat
    let height: CGFloat

    switch image.imageOrientation {
    case .left, .leftMirrored, .right, .rightMirrored:
        width = image.size.height
        height = image.size.width
    default:
        width = image.size.width
        height = image.size.height
    }

    let tileWidth = Int(width / CGFloat(howMany))
    let tileHeight = Int(height / CGFloat(howMany))

    let scale = Int(image.scale)
    var images = [UIImage]()
    let cgImage = image.cgImage!

    var adjustedHeight = tileHeight

    var y = 0
    for row in 0 ..< howMany {
        if row == (howMany - 1) {
            adjustedHeight = Int(height) - y
        }
        var adjustedWidth = tileWidth
        var x = 0
        for column in 0 ..< howMany {
            if column == (howMany - 1) {
                adjustedWidth = Int(width) - x
            }
            let origin = CGPoint(x: x * scale, y: y * scale)
            let size = CGSize(width: adjustedWidth * scale, height: adjustedHeight * scale)
            let tileCgImage = cgImage.cropping(to: CGRect(origin: origin, size: size))!
            images.append(UIImage(cgImage: tileCgImage, scale: image.scale, orientation: image.imageOrientation))
            x += tileWidth
        }
        y += tileHeight
    }
    return images
}

//MARK:- SPEAK A TEXT

var utterance = AVSpeechUtterance()
var synthesizer = AVSpeechSynthesizer()

func SpeakAText(_ text : String){
    utterance = AVSpeechUtterance(string: text)
    utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
    utterance.rate = 0.5
    if !synthesizer.isSpeaking {
    synthesizer.speak(utterance)
    }
}



//MARK:- MAKING LABEL CLICKABLE
extension UILabel {
func makeClickable(target: Any, selector: Selector){
    self.isUserInteractionEnabled = true
    let guestureRecognizer = UITapGestureRecognizer(target: target, action: selector)
    self.addGestureRecognizer(guestureRecognizer)
}
}


//MARK:- GO TO LOGIN PAGE

func gotoLoginPage(){
    DispatchQueue.main.async {
    guard let rootVC = UIStoryboard.init(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else {
        return
    }
    let domain = Bundle.main.bundleIdentifier!
    UserDefaults.standard.removePersistentDomain(forName: domain)
    UserDefaults.standard.synchronize()
    let navigationController = UINavigationController(rootViewController: rootVC)
    navigationController.isNavigationBarHidden = true
    UIApplication.shared.windows.first?.rootViewController = navigationController
    UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
