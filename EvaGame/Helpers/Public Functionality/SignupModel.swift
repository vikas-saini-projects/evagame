//
//  SignupModel.swift
//  EvaGame
//
//  Created by Techwin on 16/03/21.
//


import Foundation

// MARK: - SignupModel
struct SignupModel: Codable {
    let status: Int
    let message: String?
    let data: SignupModelDataClass?
    let method, success: String?
}

// MARK: - SignupModelDataClass
struct SignupModelDataClass: Codable {
    let userid: String
    let username: String?
    let password, user_type, email: String?
    let age, social_id: String?
    let sessionkey, devicetype, devicetoken, isactive: String?
    let created_on: String?
    let coins : String?
}

