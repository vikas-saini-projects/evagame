//
//  AppDelegate.swift
//  BaseProject
//
//  Created by Techwin on 21/12/20.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import FBSDKCoreKit
import GoogleSignIn

//@main
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    var window : UIWindow?
    //MARK:- DID FINISH LAUNCHING WITH OPTIONS : CALLED EVERYTIME WHEN APPLICATION LAUNCHED
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //UNABLE IQKEYBOARDMANAGER FOR WHOLE PROJECT & RESIGN THE KEYBOARD WHEN TOUCHED OUTSIDE THE KEYBOARD
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        //GOOGLE SIGN IN
        GIDSignIn.sharedInstance().clientID = "57279183702-t3cvusk3180so2uvng76bhi15jh4ii3h.apps.googleusercontent.com"
        //commenting as the login and signup is taken out
        // CheckForLogin()
        //PUT LOGIC BEFORE RETURN STATEMENT
       
        return true
    }
    
    
    func  CheckForLogin(){
        if UserDefaults.standard.string(forKey: UD_SESSIONKEY) != nil {
            //it means user is already logged in
            navigateToThemes()
        }
    }
    
    //Comes with Facebook
    func application(_ app: UIApplication,open url: URL,options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        ApplicationDelegate.shared.application(app,open: url,sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    
    func navigateToThemes() {
        DispatchQueue.main.async {
      
         guard let rootVC = UIStoryboard.init(name: "ChooseTheme", bundle: nil).instantiateViewController(withIdentifier: "ChooseThemeViewController") as? ChooseThemeViewController else {
             return
         }
         let navigationController = UINavigationController(rootViewController: rootVC)
         navigationController.isNavigationBarHidden = true
         UIApplication.shared.windows.first?.rootViewController = navigationController
         UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }
    
    // MARK: UISceneSession Lifecycle
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "BaseProject")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

