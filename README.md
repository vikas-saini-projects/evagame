
Eva Game is a Ipad and iphone application for kids.
(Landscape Only orientation),
(XCode 12.3),
(Swift 5),
(Mid Feb 2021),


BackGround Images : https://mail.google.com/mail/u/0/#search/Navdeep+Singh/FMfcgxwLsmbbqCmgtLCTpLFSNnTKkwJG (deepakraj.techwinlabs@gmail.com),

Designs : https://mail.google.com/mail/u/1/#search/Navdeep+singh/FMfcgxwLsdGLfNNQMdkMllZRNTBphtDq(iphone.tecwinlabs@gmail.com),

Facebook Login From : iphone.techwinlabs@gmail.com,

Google Sign in From :  hitesh.techwin@gmail.com,

ApiSheet Link: https://docs.google.com/spreadsheets/d/1JeiWlrd7ZjnSOfDK2bS7KUc6FFnQ0XVNZObiW2OrSe4/edit#gid=0,


Dated 22 March 2021 :

Client want to remove login , signup and Forgot password , So there is no need to show user Email on Profile page , there is no use of logout as well ,
We need to handle coins(rewards) collection on our end and i am going to use userDefaults for this , I asked backend to remove userid and session key from 3 api's ( named : Themes , PlayGames and third level) , 
I am pretty much sure that they will ask to add them again in near future, 

Changes i am making : 

1. making theme controller as initial view controller and remove the CheckForLogin method call from appDelegate ( did finish launching with options) , embedded a navigation controller to theme view controller

2. General >> Deployment info >> Main interface is changed from "Auth" to "Choose theme"

3. commented out the method which call the api for getting profile on profileViewController , Hide the logout button on the same controller , Coins count from userDefaults added , Email View Hidden

4. Victory Page : removed the rewards api call from the viewDidLoad , Handling the coins with userDefaults in viewDidLoad



